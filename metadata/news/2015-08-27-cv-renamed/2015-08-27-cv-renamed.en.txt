Title: sys-apps/cv renamed to sys-apps/progress
Author: Kylie McClain <somasis@exherbo.org>
Content-Type: text/plain
Posted: 2015-08-22
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: sys-apps/cv

Please install sys-apps/progress and *afterwards* uninstall sys-apps/cv.

1. Take note of any packages depending on sys-apps/cv:
cave resolve \!sys-apps/cv

2. Install sys-apps/progress:
cave resolve sys-apps/progress -x

3. Re-install the packages from step 1.

4. Uninstall sys-apps/cv:
cave resolve \!sys-apps/cv -x

Do it in *this* order or you'll potentially *break* your system.

